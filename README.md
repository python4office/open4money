# open4money

> 开源≠不赚钱，一起探索开源商业化之路。

## 介绍


欢迎开源爱好者加入本项目的编写和讨论。

初步规划，本项目主要包含以下2部分内容：

- 通过研究流行的开源项目，总结他们是怎么赚钱的
- 分享自己的经验：你的开源项目是怎么赚钱的，包含开发、推广、变现等

### 参与方式

在根目录下创建自己的文件夹，文件夹名建议用自己的名字，例如``wanfeng``，并添加自己的内容。

将写好的内容链接，根据对应的分类，加入到 ``README.md`` 中。

## 核心内容

### 流行项目



### 个人经验

- python-office
  - [我是怎么推广python-office的？](https://gitcode.com/python4office/open4money/blob/main/wanfeng/20241225-%E6%88%91%E6%98%AF%E6%80%8E%E4%B9%88%E6%8E%A8%E5%B9%BFpython-office%E7%9A%84%EF%BC%9F.md)