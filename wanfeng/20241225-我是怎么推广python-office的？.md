# 我是怎么推广python-office的？

先介绍一下这个项目：python-office，这是一个基于Python语言开发的自动化办公第三方库，集成了很多pyhon在办公领域的强大功能，比如：自动化办公、数据可视化、PDF处理等等。

项目变现的前提是更多的人知道这个项目，并且使用它。那么如何推广呢？

> 2022年初发布后，至今经过了近3年的推广，目前的效果见文章分享：[26.7万下载！Python自动化办公专用库：python-office，发布1.0.0版本](https://mp.weixin.qq.com/s/7aA0KoXGJuSFkTns-MZYjA)

我自己的经验是：别人star/使用/付费只是一个推广结果，而推广的核心工作是搭建推广体系。

下面我来介绍下我推广python-office的经验。

## 推广体系的搭建

如果现在让你推广一个新品牌的饮料，你就会发现饮料被别人购买的前提有2个：

- 铺天盖地都是这款饮料的广告：决定了他能不能想起来买你的饮料。
- 你的饮料好喝：决定了他会不会继续买。

而搭建开源项目的推广体系也就是起到第一个作用：通过宣传，让他看到你的项目，有需要的时候能想起来。

怎么让你的项目铺天盖地都是你的项目信息呢？我搭建了以下推广体系：

- 开通了全网同名的自媒体账号：Python自动化办公社区，然后保持更新。目前公众号10w+粉丝，B站14w+粉丝，小红书1w+粉丝。
- 开发了2个网站：[python4office.cn](python4office.cn)和[python-office.com](python-office.com) 
- 录制了一套课程：[给小白的《50讲 · Python自动化办公》](https://www.python-office.com/course/50-python-office.html)，目前全网累计200w+播放。
- 参加线下活动，例如：Python社区活动、PyCon中国等，通过演讲分享自己的项目。今年在Python中国的演讲：[Python中国演讲：非程序员如何学习和使用 Python？](https://mp.weixin.qq.com/s/pJAOgaQ8vA08NrNpJzngFw)
- 通过经验分享，帮助小博主围绕python-office录制课程，小博主能得到流量和变现，我能得到推广。

## 推广体系的作用

推广体系搭建起来以后，你就可以专注于项目本身和这个体系的不断完善，不用花时间去关注推广了多少star、变现了多少money。



